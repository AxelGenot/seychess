//Inserting the Icons

let squares = [...document.querySelectorAll('.square')];


function insertIcons() {
    squares.forEach(square => {
        if (square.innerText.length !== 0) {
            if (square.innerText == 'Wpawn' || square.innerText == 'Bpawn') {
                square.innerHTML = `${square.innerText}  <img class='icons pawn first' draggable="true" id='${square.innerText}' src="./icons/${square.innerText}.png" alt="${square.innerText}">`
                square.style.cursor = 'pointer'

            }
            else if (square.innerText == 'Wbishop' || square.innerText == 'Bbishop') {
                square.innerHTML = `${square.innerText} <img class='icons bishop' draggable="true" id='${square.innerText}' src="./icons/${square.innerText}.png" alt="${square.innerText}">`
                square.style.cursor = 'pointer'

            }
            else if (square.innerText == 'Wqueen' || square.innerText == 'Bqueen') {
                square.innerHTML = `${square.innerText} <img class='icons queen' draggable="true" id='${square.innerText}' src="./icons/${square.innerText}.png" alt="${square.innerText}">`
                square.style.cursor = 'pointer'

            }
            else if (square.innerText == 'Wking' || square.innerText == 'Bking') {
                square.innerHTML = `${square.innerText} <img class='icons king' draggable="true" id='${square.innerText}' src="./icons/${square.innerText}.png" alt="${square.innerText}">`
                square.style.cursor = 'pointer'

            }
            else if (square.innerText == 'Wknight' || square.innerText == 'Bknight') {
                square.innerHTML = `${square.innerText} <img class='icons knight' draggable="true" id='${square.innerText}' src="./icons/${square.innerText}.png" alt="${square.innerText}">`
                square.style.cursor = 'pointer'

            }

            else {

                square.innerHTML = `${square.innerText} <img class='icons rook' draggable="true" id='${square.innerText}' src="./icons/${square.innerText}.png" alt="${square.innerText}">`
                square.style.cursor = 'pointer'
            }
        }

    })

};

insertIcons();




//Coloring Squares of the Board


const lightSquares = [...document.querySelectorAll('.square.lt')];
const darkSquares = [...document.querySelectorAll('.square.dk')];
let lightColor = '#DCA98D';
let darkColor = '#814221';
lightSquares.forEach(square => square.style.backgroundColor = lightColor);
darkSquares.forEach(square => square.style.backgroundColor = darkColor);



//Defining the pieces one by one by selecting ID


const pieces = [...document.querySelectorAll('img')];
const bKing = document.querySelector('#Bking');
const wKing = document.querySelector('#Wking');
const bQueen = document.querySelector('#Bqueen');
const wQueen = document.querySelector('#Wqueen');

const bPawns = [...document.querySelectorAll('#Bpawn')];
for (let i = 0; i < bPawns.length; i++) {
    bPawns[i].id = `Bpawn${i + 1}`;
};
const wPawns = [...document.querySelectorAll('#Wpawn')];
for (let i = 0; i < wPawns.length; i++) {
    wPawns[i].id = `Wpawn${i + 1}`;
};

const bBishops = [...document.querySelectorAll('#Bbishop')];
for (let i = 0; i < bBishops.length; i++) {
    bBishops[i].id = `Bbishop${i + 1}`;
};

const wBishops = [...document.querySelectorAll('#Wbishop')];
for (let i = 0; i < wBishops.length; i++) {
    wBishops[i].id = `Wbishop${i + 1}`;
};
const bRooks = [...document.querySelectorAll('#Brook')];
for (let i = 0; i < bRooks.length; i++) {
    bRooks[i].id = `Brook${i + 1}`;
};

const wRooks = [...document.querySelectorAll('#Wrook')];
for (let i = 0; i < wRooks.length; i++) {
    wRooks[i].id = `Wrook${i + 1}`;
};

const bKnights = [...document.querySelectorAll('#Bknight')];
for (let i = 0; i < bKnights.length; i++) {
    bKnights[i].id = `Bknight${i + 1}`;
};

const wKnights = [...document.querySelectorAll('#Wknight')];
for (let i = 0; i < wKnights.length; i++) {
    wKnights[i].id = `Wknight${i + 1}`;
};






//SETTING DRAGGABLE ASSET FOR DRAGGED PIECE


for (let piece of pieces) {
    piece.addEventListener('dragstart', () => {
        piece.classList.add('dragging');
        const draggable = document.querySelector('.dragging');
        mooving(draggable);
    });

    piece.addEventListener('dragend', () => {
        piece.classList.remove('dragging')
        if (piece.classList.contains('first')) {
            piece.classList.remove('first');
        }
    })
}

//DEFINING MOOVEMENTS FOR EACH PIECE

function mooving(draggable) {
    let pieceID = draggable.parentElement.id.split('');
    console.log(draggable.id);
    console.log(pieceID);
    for (let square of squares) {
        if (draggable.id.includes('Bpawn') === true) {
            //Black Pawns
            if (pieceID[1] - square.id.split('')[1] === 1 && pieceID[0] === square.id.split('')[0]) {
                square.addEventListener('dragover', (e) => {
                    e.preventDefault();
                    square.appendChild(draggable);
                    console.log(square.id)
                })
            } else if (pieceID[1] - square.id.split('')[1] === 2 && pieceID[0] === square.id.split('')[0]) {
                if (draggable.classList.contains('first')) {
                    square.addEventListener('dragover', (e) => {
                        e.preventDefault();
                        square.appendChild(draggable);
                        console.log(square.id)
                    })
                } else {

                }
            } else {

            }

        }

        else if (draggable.id.includes('Wpawn') === true) {
            //White Pawns
        }

    };
}










