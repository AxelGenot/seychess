// RENDERING THE SQUARES DRAGGABLE

let squares = [...document.querySelectorAll('.square')];
const icons = [...document.querySelectorAll('.icon')];

icons.forEach(icon => {
    icon.addEventListener('dragstart', e => {
        e.dataTransfer.setData('text/plain', icon.id);
    });
});

squares.forEach(square => {

    square.addEventListener('dragover', e => {
        e.preventDefault();
        square.classList.add('drag-over');
    });

    square.addEventListener('dragleave', e => {
        square.classList.remove('drag-over');
    })

    square.addEventListener('drop', e => {
        e.preventDefault();

        const droppedIconId = e.dataTransfer.getData('text/plain');
        const droppedIcon = document.getElementById(droppedIconId);
        square.appendChild(droppedIcon);
        square.classList.remove('drag-over');

    })

})
